## Unidad2
Cree un programa que entrege el *n-esimo* numero de la serie de Fibonacci. El valor de *n* estara en el rango del 1 al 100. 

Ejemplo
```bash
Entrada
n = 3
salida
2

Entrada
n = 9
salida
34

Entrada
n = 2
salida
1
```
Tome en cuenta lo siguiente:
- La sucesión de Fibonacci se explica en el siguiente video:
https://www.youtube.com/watch?v=yDyMSliKsxI

- El numero *n* es un numero cualquiera entre 1 y 100


### Condiciones de entrega
Subira el codigo a su cuenta de github, el cual tendra lo siguiente

nombre: main[numerodecontrol].java,

ejemplo: retomain12161219.java

